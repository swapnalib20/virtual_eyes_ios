//
//  Utilities.swift
//  VirtualEyes
//
//  Created by Waaheeda on 21/06/19.
//  Copyright © 2019 Waaheeda. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import MediaPlayer

class  Utilities
{
    static let synthesizer : AVSpeechSynthesizer = AVSpeechSynthesizer.init()
    
    static func  speakNow(message : String, completion : @escaping ()->Void)
    {
        DispatchQueue.main.async {
            Utilities.setSystemVolume(volume: 1.0)
            let utterance = AVSpeechUtterance.init(string: message)
            Utilities.synthesizer.speak(utterance)
            completion()
        }
    }
    static func setSystemVolume(volume: Float)
    {
        let volumeView = MPVolumeView()
        var MAX_VOLUME : Float = 0.5
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        DispatchQueue.main.async {
            volumeView.frame = CGRect.zero
            volumeView.showsRouteButton = false
            volumeView.showsVolumeSlider = false
            volumeView.isHidden = true
            slider?.isHidden = true
        }
        let route = AVAudioSession.sharedInstance().currentRoute
        for desc : AVAudioSessionPortDescription in route.outputs
        {
            /* if desc.portType.elementsEqual(AVAudioSessionPortHeadphones)
             {
             MAX_VOLUME = 0.5
             break
             }
             else
             {*/
            MAX_VOLUME = volume
            //}
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.05)
        {
            slider?.value =  MAX_VOLUME
            volumeView.willMove(toWindow: UIApplication.shared.keyWindow)
            volumeView.didMoveToSuperview()
        }
    }
}
